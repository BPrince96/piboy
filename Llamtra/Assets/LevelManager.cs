﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {


    public void loadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

	public void loadFirstLevel()
    {
        SceneManager.LoadScene(1);
    }
}
