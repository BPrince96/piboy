﻿using UnityEngine;
using System.Collections;

public class enemyController : MonoBehaviour {

    Rigidbody2D body;
    bool onGround = false;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;

    public float maxSpeed = 1.0f;


    public Transform gunbarrel;
    public GameObject enemyBullet;
    public float fireRate = 2.0f;
    public float nextFire = 1.0f;

    bool facingRight = true;

    public GameObject player;
    Vector2 toPlayer;
    public float detectionRadius = 100f;
    public float bulletSpeed = 2.0f;
    public float bulletLifetime = 3.0f;

    public float walkCycleTime = 2.0f;
    public float walkCycleSpeed = 2.0f;

    // Use this for initialization
    void Start () {
        body = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        toPlayer = player.transform.position - this.gameObject.transform.position;

        if (toPlayer.magnitude < detectionRadius)
        {
            shoot();
        }

        if (Time.time > walkCycleTime)
        {
            walkCycleTime = Time.time + walkCycleSpeed;
            maxSpeed *= -1;
            flipCharacter();
        }

        body.velocity = new Vector2(maxSpeed, body.velocity.y);
    }

    public void kill()
    {
        Destroy(this.gameObject);
    }

    void shoot()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            GameObject bullet = (GameObject)GameObject.Instantiate(enemyBullet, transform.position, new Quaternion());
            bullet.GetComponent<Rigidbody2D>().AddForce(toPlayer.normalized * bulletSpeed);
            Destroy(bullet, bulletLifetime);
        }
    }

    void flipCharacter()
    {
        facingRight = !facingRight;

        Vector3 scale = transform.localScale;
        scale.x *= -1;

        transform.localScale = scale;
    }
}
