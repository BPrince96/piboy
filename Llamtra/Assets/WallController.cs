﻿using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour {

    Rigidbody2D body;
    public float maxSpeed = 1.0f;
    public Transform playerRef;
    public Transform movePoint;

    // Use this for initialization
    void Start () {

        body = this.gameObject.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void FixedUpdate() {

        float move = Input.GetAxis("Horizontal");

        if (move > 0 && playerRef.position.x >= movePoint.position.x)
        {
            body.velocity = new Vector2(move * maxSpeed, body.velocity.y);
        }
        else
        {
            body.velocity = new Vector2(0, 0);
        }

    }
}
