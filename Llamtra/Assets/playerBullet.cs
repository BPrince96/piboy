﻿using UnityEngine;
using System.Collections;

public class playerBullet : MonoBehaviour {

    public float speed = 1.0f;
    public float LifeTime = 1.0f;

    Rigidbody2D body;

    // Use this for initialization
    void Start()
    {
        Destroy(gameObject, LifeTime);

        body = this.gameObject.GetComponent<Rigidbody2D>();
        if(transform.localRotation.z == 0)
        {
            body.AddForce(new Vector2(1, 0) * speed, ForceMode2D.Impulse);
        }
        else
        {
            if (transform.localRotation.z == 1)
            {
                body.AddForce(new Vector2(-1, 0) * speed, ForceMode2D.Impulse);
            }
            else
            {
                if(transform.localRotation.z > 0)
                {
                    body.AddForce(new Vector2(0, 1) * speed, ForceMode2D.Impulse);
                }
            }
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy")
        {
            other.gameObject.GetComponent<enemyController>().kill();
        }
        
    }
}
