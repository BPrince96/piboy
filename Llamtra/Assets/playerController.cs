﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour {

    Rigidbody2D body;
    bool onGround = false;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpForce = 1.0f;

    public float maxSpeed = 1.0f;


    public Transform gunbarrel;
    public Transform fireUp;
    public GameObject bullet;
    float fireRate = 1.0f;
    float nextFire = 0.5f;

    bool facingRight = true;

    public Sprite normalSprite;
    public Sprite crouchingSprite;
    SpriteRenderer imageController;
    CircleCollider2D normalCollider;
    BoxCollider2D crouchingCollider;

    AudioSource audioSource;

	// Use this for initialization
	void Start () {

        body = this.gameObject.GetComponent<Rigidbody2D>();

        imageController = this.gameObject.GetComponent<SpriteRenderer>();

        normalCollider = this.gameObject.GetComponent<CircleCollider2D>();
        crouchingCollider = this.gameObject.GetComponent<BoxCollider2D>();

        audioSource = this.gameObject.GetComponent<AudioSource>();
    }
	
    void Update()
    {
        if (onGround && Input.GetAxis("Jump") > 0)
        {
            body.AddForce(new Vector2(0, jumpForce));
        }

        if(Input.GetAxisRaw("Fire1") > 0)
        {
            shoot();
        }

        if(Input.GetAxis("Vertical") < 0)
        {
            normalCollider.enabled = false;
            crouchingCollider.enabled = true;
            imageController.sprite = crouchingSprite;
        }
        else
        {
            normalCollider.enabled = true;
            crouchingCollider.enabled = false;
            imageController.sprite = normalSprite;
        }
    }      

    void FixedUpdate()
    {
        onGround = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);

        float move = Input.GetAxis("Horizontal");
        body.velocity = new Vector2(move * maxSpeed, body.velocity.y);

        if(move > 0 && !facingRight)
        {
            flipCharacter();
        }
        else
        {
            if(move < 0 && facingRight)
            {
                flipCharacter();
            }
        }
    }

    void flipCharacter()
    {
        facingRight = !facingRight;

        Vector3 scale = transform.localScale;
        scale.x *= -1;

        transform.localScale = scale;
    }

    public void kill()
    {
        Destroy(this.gameObject);
    }

    void shoot()
    {
        if(Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;

            audioSource.Play();

            if(facingRight && Input.GetAxis("Vertical") == 0)
            {
                Instantiate(bullet, gunbarrel.position, Quaternion.Euler(new Vector3(0, 0, 0)));
            }
            else
            {
                if(!facingRight && Input.GetAxis("Vertical") == 0)
                {
                    Instantiate(bullet, gunbarrel.position, Quaternion.Euler(new Vector3(0, 0, 180f)));
                }
                else
                {
                    Instantiate(bullet, fireUp.position, Quaternion.Euler(new Vector3(0, 0, 90f)));
                }
                
            }
        }
    }
}
